djangocms-unimelb 
==============================

A Django CMS app using the University of Melbourne's web framework.

:License: Apache Software License 2.0

Installation
------------

Install using pip::

    pip install git+https://gitlab.unimelb.edu.au/mdap-public/djangocms-unimelb.git

Use the Django CMS installer

Follow the instructions for installing Django CMS

Add to your installed apps::

    INSTALLED_APPS = [
        ...
        "djangocms_unimelb",
        ...
    ]



Further Reading
---------------

For more information on the components in the University of Melbourne's web framework see https://web.unimelb.edu.au.



