#!/usr/bin/env python3

import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tests.test_settings")
    from django.core.management import execute_from_command_line
    args = sys.argv[:1] + ["makemigrations"]
    if len(sys.argv) > 1 and sys.argv[1] == "--empty":
        args += ["--empty", "djangocms_unimelb"]
    execute_from_command_line(args)
