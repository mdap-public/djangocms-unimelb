from django.test.client import RequestFactory
from cms.api import add_plugin
from django.test import TestCase
from cms.models import Placeholder
from cms.plugin_rendering import ContentRenderer
import inspect

import djangocms_unimelb.cms_plugins as cms_plugins

from .render_test_case import RenderTestCase, test_data_dir

class PluginTestCase(RenderTestCase):
    def render(self, **kwargs):
        placeholder = Placeholder.objects.create(slot='test')
        model_instance = add_plugin(
            placeholder=placeholder,
            plugin_type=self.model,
            language='en',
            **kwargs
        )
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(model_instance, {})
        return html
        
    def subdir_name(self):
        return test_data_dir(subdir=type(self).__name__)


class UnimelbBlockListingPluginTestCase(PluginTestCase):
    model = cms_plugins.UnimelbBlockListingPlugin

    def test_block_listing(self):
        self.assert_render_equals(
            filename="test_block_listing.html",
        )


class UnimelbBlockListingItemNewsPluginTestCase(PluginTestCase):
    model = cms_plugins.UnimelbBlockListingItemNewsPlugin

    def test_block_listing_heading(self):
        self.assert_render_equals(
            heading="Heading Text",
            filename="test_block_listing_heading.html",
        )
    
    def test_block_listing_text(self):
        self.assert_render_equals(
            heading="Heading Text",
            text="My text",
            filename="test_block_listing_text.html",
        )

    def test_meta_right(self):
        self.assert_render_equals(
            heading="Heading Text",
            text="My text",
            meta_right="Right Meta",
            filename="test_meta_right.html",
        )

    def test_meta_left(self):
        self.assert_render_equals(
            heading="Heading Text",
            text="My text",
            meta_right="Left Meta",
            filename="test_meta_left.html",
        )


class UnimelbBlockListingImagePluginTestCase(PluginTestCase):
    model = cms_plugins.UnimelbBlockListingImagePlugin

    def test_block_listing(self):
        self.assert_render_equals(
            filename="test.html",
            external_picture="http://example.com/image.jpg"
        )



class UnimelbBlockquotePluginTestCase(PluginTestCase):
    model = cms_plugins.UnimelbBlockquotePlugin

    def test_text(self):
        self.assert_render_equals(
            text="The history of the race...",
            filename="test_text.html",
        )

    def test_classic(self):
        self.assert_render_equals(
            text="Quote Text",
            style="classic",
            filename=f"{inspect.currentframe().f_code.co_name}.html",
        )

    def test_left(self):
        self.assert_render_equals(
            text="Quote Text",
            style="left",
            filename=f"{inspect.currentframe().f_code.co_name}.html",
        )

    def test_right(self):
        self.assert_render_equals(
            text="Quote Text",
            style="right",
            filename=f"{inspect.currentframe().f_code.co_name}.html",
        )

    def test_cite(self):
        self.assert_render_equals(
            filename=f"{inspect.currentframe().f_code.co_name}.html",
            text="Quote Text",
            cite="Person Name",
        )


class UnimelbUnimelbFigurePluginTestCase(PluginTestCase):
    model = cms_plugins.UnimelbFigurePlugin

    def test_external(self):
        self.assert_render_equals(
            filename=f"{inspect.currentframe().f_code.co_name}.html",
            external_picture="http://www.example.com/image.jpg",
        )
