from .render_test_case import RenderTestCase


class UnimelbTemplateTagsTestCase(RenderTestCase):

    def test_404(self):
        self.assert_render_equals(
            string="""
            {% load unimelb %}
            {% unimelb_404 %}
            """,
            filename="test_404.html",
        )
