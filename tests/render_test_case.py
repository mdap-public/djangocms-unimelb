from pathlib import Path
from django.test import TestCase
from django.template import Context, Template


def test_data_dir(subdir=""):
    path = Path(__file__).parent/"testdata"
    if subdir:
        path = path/subdir
    path.mkdir(exist_ok=True, parents=True)
    return path


class RenderTestCase(TestCase):
    model = None

    def render(self, string, context=None):
        """ 
        taken from https://stackoverflow.com/a/1690879 
        
        function can be overridden if there needs to be a specific type of rendering.
        """
        context = context or {}
        context = Context(context)
        return Template(string).render(context)

    def subdir_name(self):
        return test_data_dir(subdir=type(self).__name__)

    def assert_render_equals(self, filename, generate=False, **kwargs):
        html = self.render(**kwargs)
        path = self.subdir_name()/filename
        if generate:
            with open(path, 'w') as f:
                f.write(html)

        with open(path, 'r') as f:
            file_string = f.read()
        
        self.assertEqual( html, file_string )