from django.apps import AppConfig


class DjangocmsUnimelbConfig(AppConfig):
    name = 'djangocms_unimelb'
