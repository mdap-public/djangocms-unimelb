from django.utils.translation import gettext_lazy as _

TAB_TYPE_CHOICES = (
    ('full-width', _('Full Width')),
    ('full-width-black', _('Full Width Black')),
    ('sidebar', _('Sidebar')),
    ('in-page', _('In Page')),
)

STAFF_LISTING_CHOICES = (
    ('default', _('Default')),
    ('min', _('Min')),
    ('detailed', _('Detailed')),
)