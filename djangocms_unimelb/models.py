import re
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from filer.fields.image import FilerImageField
from cms.models import CMSPlugin
from djangocms_link.models import AbstractLink
from djangocms_text_ckeditor.fields import HTMLField
from djangocms_picture.models import AbstractPicture
from cms.models.fields import PlaceholderField
from filer.fields.file import FilerFileField

from .constants import TAB_TYPE_CHOICES, STAFF_LISTING_CHOICES

class UnimelbTab(CMSPlugin):
    """
    Creates a tab using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs

    Based on https://github.com/django-cms/djangocms-bootstrap4/blob/master/djangocms_bootstrap4/contrib/bootstrap4_tabs/
    """
    tab_type = models.CharField(
        verbose_name=_('Type'),
        choices=TAB_TYPE_CHOICES,
        default=TAB_TYPE_CHOICES[0][0],
        max_length=255,
    )
    tab_index = models.PositiveIntegerField(
        verbose_name=_('Index'),
        null=True,
        blank=True,
        help_text=_('Index of element to open on page load starting at 1.'),
        default=1,
    )

    def __str__(self):
        return f"{self.tab_type}"

    def get_short_description(self):
        text = '({})'.format(self.tab_type)

        return text


class UnimelbTabItem(CMSPlugin):
    """
    Creates an item in a tab using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs
    """
    tab_title = models.CharField(
        verbose_name=_('Tab title'),
        max_length=255,
    )

    def __str__(self):
        return self.tab_title

    def get_short_description(self):
        return self.tab_title


class UnimelbStaffListing(CMSPlugin):
    """
    Adds a listing of staff members

    https://web.unimelb.edu.au/components/people 
    """
    listing_type = models.CharField(
        verbose_name=_('Type'),
        choices=STAFF_LISTING_CHOICES,
        default=STAFF_LISTING_CHOICES[0][0],
        max_length=255,
    )

    def __str__(self):
        return f"{self.pk}"


class UnimelbStaffMember(CMSPlugin):
    """
    Adds a listing of staff members

    https://web.unimelb.edu.au/components/people 
    """
    name = models.CharField(max_length=255)
    url = models.CharField(
        verbose_name=_('URL'),
        max_length=1023,
        blank=True,
        default="",
    )
    image = FilerImageField(
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text="An image of this staff member."
    )
    image_external_url = models.CharField(
        max_length=1023,
        blank=True,
        default="",
        help_text="An external URL to an image for this staff member. This is only used if the 'image' field is empty.",
    )
    title = models.CharField(max_length=255, blank=True, default="")
    institution = models.CharField(max_length=255, blank=True, default="", help_text="e.g. University of Melbourne")

    def __str__(self):
        return self.name

    def image_url(self):
        if self.image:
            return self.image.url
        return self.image_external_url


class UnimelbHeading(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/headings
    """
    text = models.CharField(max_length=255, blank=True, help_text="If blank then it takes the title of the current page or tab item.")
    aligned = models.BooleanField(default=False, blank=True)
    padding_top = models.CharField(max_length=255, blank=True, help_text="e.g. 2.0rem")

    def __str__(self):
        return self.text


class UnimelbHeader(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/headers
    """
    heading = models.CharField(max_length=255, default="", blank=True, help_text="The heading for the banner.")
    subline = models.CharField(max_length=1023, default="", blank=True)
    image = FilerImageField(
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text="The image to use in the background of the banner."
    )
    tint = models.CharField(
        max_length=255, 
        default="", 
        blank=True,
        help_text="A colour to tint the header image. e.g. rgba(16, 41, 80, 0.7)",
    )
    messages = models.BooleanField(default=True,help_text="Displays alert messages underneath header.")

    def __str__(self):
        if self.heading:
            return self.heading
        if self.subline:
            return self.subline
        if self.image:
            return str(self.image)
        return str(self.pk)


class UnimelbFooter(CMSPlugin):
    light = models.BooleanField(default=False, help_text="If the background colour should be light.")


IconChoices = models.TextChoices('IconChoices', 'add bar bike bus cafe campaign car cart chat chevron-right check check-circle city clock close computer cutlery delete devices download edit ellipsis face facebook faculty flight group home hr instagram jobs library linkedin location lock mail map menu money pharmacy phone print profile rss search send share smartphone student tag train twitter vimeo walk world youtube zoom-in zoom-out question info info-outline'.title())


class UnimelbFooterLink(AbstractLink):
    text = models.CharField(max_length=31, default="", blank=True, help_text="The text to accompany the link.")
    icon = models.CharField(max_length=31, default="", blank=True, choices=IconChoices.choices)


class UnimelbBlockListing(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/block-listing
    """
    def __str__(self):
        return str(self.pk)


class MediaLink(AbstractLink):
    file = FilerFileField(
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    class Meta:
        abstract = True

    def get_link(self):
        link = super().get_link()

        if not link and self.file:
            link = self.file.url

        return link


class OptionalPicture(AbstractPicture):
    class Meta:
        abstract = True
    
    def clean(self): 
        try:
            super().clean()
        except ValidationError as err:
            # TODO Check message
            pass


BlockListingCategory = models.TextChoices('BlockListingCategory', 'news newshero exhibition event instagram twitter')
ImageLocation = models.TextChoices('ImageLocation', 'DEFAULT TOP BACKGROUND')


class UnimelbBlockListingItem(OptionalPicture):
    """
    https://web.unimelb.edu.au/components/block-listing

    See parent: https://github.com/django-cms/djangocms-link/blob/master/djangocms_link/models.py
    """
    category = models.CharField(max_length=10, default=BlockListingCategory.news, choices=BlockListingCategory.choices)
    heading = models.CharField(max_length=255, default="", blank=True, help_text="The heading for the block listing.")
    minor_heading = models.CharField(max_length=255, default="", blank=True, help_text="A secondary heading for the block listing.")
    text = HTMLField(default="", blank=True, help_text="The text for the block listing. Only a few lines are visible.")
    call_to_action = models.CharField(max_length=255, default="", blank=True, help_text="The text in a call to action button.")
    meta_left = models.CharField(max_length=255, default="", blank=True, help_text="The text at the bottom left.")
    meta_right = models.CharField(max_length=255, default="", blank=True, help_text="The text at the bottom right.")
    double = models.BooleanField(default=False, help_text="Whether or not this block listing should take up two boxes.")
    image_location = models.CharField(max_length=10, default=ImageLocation.DEFAULT, choices=ImageLocation.choices, help_text="The location of the image (if present).")
    link_is_optional = True
    file = FilerFileField(
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

    def __str__(self):
        if self.heading:
            return self.heading
        if self.text:
            return self.text
        if self.meta_left:
            return str(self.meta_left)
        return str(self.pk)

    def get_short_description(self):
        return str(self)

    def image_in_backround(self):
        if not self.img_src:
            return False
        if self.image_location == ImageLocation.DEFAULT:
            return self.category in ('newshero', 'instagram')
        return self.image_location == ImageLocation.BACKGROUND

    def image_at_top(self):
        if not self.img_src:
            return False
        return not self.image_in_backround()

    def get_link(self):
        link = super().get_link()

        if not link and self.file:
            link = self.file.url

        return link


    
class UnimelbPathfinder(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/pathfinder
    """
    def __str__(self):
        return str(self.pk)

    def number(self):
        return len(child_plugin_instances)


class UnimelbPathfinderBox(AbstractLink):
    """
    https://web.unimelb.edu.au/components/pathfinder

    See parent: https://github.com/django-cms/djangocms-link/blob/master/djangocms_link/models.py
    """
    heading = models.CharField(max_length=255, default="", blank=True, help_text="The heading for the pathfinder box.")
    text = models.CharField(max_length=1023, default="", blank=True, help_text="The text for the pathfinder box.")
    button = models.CharField(max_length=255, default="", blank=True, help_text="The text for the button.")

    def __str__(self):
        if self.heading:
            return self.heading
        if self.text:
            return self.text
        if self.button:
            return str(self.button)
        return str(self.pk)

    def get_short_description(self):
        return str(self)


class UnimelbSearchForm(AbstractLink):
    """
    https://web.unimelb.edu.au/components/search
    """
    pass


class UnimelbSearchResults(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/search
    https://github.com/etianen/django-watson
    """
    pagination = models.PositiveIntegerField(default=30)

    def __str__(self):
        return str(self.pk)


class UnimelbAccordion(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/accordion
    https://resources.web.unimelb.edu.au/web-content/creating-content/how-to-use-accordions-well
    """
    def __str__(self):
        return str(self.pk)


class UnimelbAccordionItem(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/accordion
    https://resources.web.unimelb.edu.au/web-content/creating-content/how-to-use-accordions-well
    """
    title = models.CharField(max_length=255)
    visible = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return str(self.title)


class UnimelbTimeline(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/timeline
    https://www.w3schools.com/tags/tag_dl.asp
    """
    def __str__(self):
        return str(self.pk)


class UnimelbTimelineItem(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/timeline
    https://www.w3schools.com/tags/tag_dl.asp

    Shows the 'time' string before the 'bold' string'.
    """
    time = models.CharField(max_length=255, default="", blank=True)
    bold = models.CharField(max_length=255, default="", blank=True)

    def __str__(self):
        return re.sub( r"\s+", " ", self.time + " " + self.bold)


class UnimelbMessages(CMSPlugin):
    """
    https://docs.djangoproject.com/en/3.2/ref/contrib/messages/
    https://web.unimelb.edu.au/components/notices
    
    Displays a message
    """
    pass


class Status(models.TextChoices):
    UNPUBLISHED = 'UN', 'Unpublished'
    PUBLISHED = 'PB', 'Published'


NoticeType = models.TextChoices('NoticeType', 'success info warning danger')

class UnimelbNotice(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/notices
    """
    notice_type = models.CharField(max_length=7, choices=NoticeType.choices)
    dismiss = models.BooleanField(default=False, help_text="Displays a button to dismiss the notice.")

    def __str__(self):
        return self.notice_type


class UnimelbLead(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/text
    """
    pass


BlockquoteStyle = models.TextChoices('BlockquoteStyle', 'classic left right')

class UnimelbBlockquote(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/text
    """
    text = models.CharField(max_length=1023, default="", blank=True)
    cite = models.CharField(max_length=255, default="", blank=True)
    style = models.CharField(max_length=7, choices=BlockquoteStyle.choices, blank=True, default="")

    def __str__(self):
        return self.text

    def css_class(self):
        if self.style == "classic":
            return "blockquote-classic"
        return self.style


class FigureSize(models.TextChoices):
    DEFAULT = '', 'Default'
    MIN = 'min', 'Min'
    MAX = 'max', 'Max'


class FigureInset(models.TextChoices):
    DEFAULT = '', 'Default'
    LEFT = 'left', 'Left'
    RIGHT = 'right', 'Right'


class UnimelbFigure(AbstractPicture, CMSPlugin):
    """
    https://web.unimelb.edu.au/components/figure
    """
    confined = models.BooleanField(default=False, help_text="Should the figure be confined to align with the text.")
    caption = HTMLField(default="", blank=True)
    size = models.CharField(max_length=3, choices=FigureSize.choices, blank=True, default=FigureSize.DEFAULT, help_text="If the figure should shrink to fit its content (min) or expand to its maximum width (max).")
    inset = models.CharField(max_length=5, choices=FigureInset.choices, blank=True, default=FigureInset.DEFAULT, help_text="If the figure should be on the left or the right.")

    def caption_no_paragraph(self):
        return re.sub(r"^\<p\>(.*)\<\/p\>$", r"\1", self.caption )


class UnimelbFigureDiv(CMSPlugin):
    """
    https://web.unimelb.edu.au/components/figure
    """
    clearfix = models.BooleanField(default=True, help_text="If you don't want the figures to overflow into the content that follows.")


class UnimelbArticle(models.Model):
    """
    https://web.unimelb.edu.au/components/news
    """
    title = models.CharField(max_length=255, help_text="The title of this article to be displayed as a heading and in the listing.")
    description = models.CharField(max_length=1023, help_text="A description of this article to be displayed as a summary in the listing.")
    content = PlaceholderField('content')