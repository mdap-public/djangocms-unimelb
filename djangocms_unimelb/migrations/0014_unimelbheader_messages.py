# Generated by Django 3.1.3 on 2021-06-08 08:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djangocms_unimelb', '0013_unimelbmessages'),
    ]

    operations = [
        migrations.AddField(
            model_name='unimelbheader',
            name='messages',
            field=models.BooleanField(default=True, help_text='Displays alert messages underneath header.'),
        ),
    ]
