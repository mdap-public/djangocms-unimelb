# Generated by Django 3.1.3 on 2021-07-31 01:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('djangocms_unimelb', '0020_auto_20210719_1811'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unimelbstaffmember',
            old_name='image',
            new_name='image_external_url',
        ),
    ]
