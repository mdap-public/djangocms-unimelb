# Generated by Django 3.1.3 on 2021-05-13 12:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('cms', '0022_auto_20180620_1551'),
        ('djangocms_unimelb', '0005_auto_20210513_2113'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnimelbHeader',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='djangocms_unimelb_unimelbheader', serialize=False, to='cms.cmsplugin')),
                ('heading', models.CharField(blank=True, default='', help_text='The heading for the banner.', max_length=255)),
                ('subline', models.CharField(blank=True, default='', max_length=1023)),
                ('image', filer.fields.image.FilerImageField(blank=True, default=None, help_text='The image to use in the background of the banner.', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.FILER_IMAGE_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
