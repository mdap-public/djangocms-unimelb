# Generated by Django 3.1.3 on 2021-05-12 12:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnimelbTab',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='djangocms_unimelb_unimelbtab', serialize=False, to='cms.cmsplugin')),
                ('tab_type', models.CharField(choices=[('full-width', 'Full Width'), ('full-width-black', 'Full Width Black'), ('sidebar', 'Sidebar'), ('in-page', 'In Page')], default='full-width', max_length=255, verbose_name='Type')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='UnimelbTabItem',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='djangocms_unimelb_unimelbtabitem', serialize=False, to='cms.cmsplugin')),
                ('tab_title', models.CharField(max_length=255, verbose_name='Tab title')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
