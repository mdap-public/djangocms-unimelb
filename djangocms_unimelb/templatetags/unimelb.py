from django import template
from django.core.paginator import EmptyPage
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.conf import settings

from classytags.helpers import InclusionTag

register = template.Library()

@register.inclusion_tag('djangocms_unimelb/paginator.html', takes_context=True)
def unimelb_paginator(context, adjacent_pages=3):
    """
    Inclusion tag for rendering a paginator for multi-page lists.

    Adapted from https://romanvm.pythonanywhere.com/post/bootstrap4-based-pagination-django-listview-30/

    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.

    :param context: parent template context
    :param adjacent_pages: the number of pages adjacent to the current
    :return: context for rendering paginator html code
    """
    start_page = max(context['page_obj'].number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = context['page_obj'].number + adjacent_pages + 1
    if end_page >= context['paginator'].num_pages - 1:
        end_page = context['paginator'].num_pages + 1
    page_numbers = [n for n in range(start_page, end_page) if n in range(1, context['paginator'].num_pages + 1)]
    page_obj = context['page_obj']
    paginator = context['paginator']
    try:
        next_ = context['page_obj'].next_page_number()
    except EmptyPage:
        next_ = None
    try:
        previous = context['page_obj'].previous_page_number()
    except EmptyPage:
        previous = None
    return {
        'page_obj': page_obj,
        'paginator': paginator,
        'page': context['page_obj'].number,
        'pages': context['paginator'].num_pages,
        'page_numbers': page_numbers,
        'next': next_,
        'previous': previous,
        'has_next': context['page_obj'].has_next(),
        'has_previous': context['page_obj'].has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': context['paginator'].num_pages not in page_numbers,
        'request': context['request']
    }


class PageMetaInclusionTag(InclusionTag):
    push_context=True
    def render_tag(self, context, **kwargs):
        if 'djangocms_page_meta' in settings.INSTALLED_APPS:
            return super().render_tag(context, **kwargs)
        return ""


class UnimelbPageMeta(PageMetaInclusionTag):
    name = "unimelb_page_meta"
    template = 'djangocms_unimelb/page_meta.html'

register.tag(UnimelbPageMeta)


class UnimelbSchemaOrgHTMLScope(PageMetaInclusionTag):
    name = "unimelb_schemaorg_html_scope"
    template = 'djangocms_unimelb/schemaorg_html_scope.html'

register.tag(UnimelbSchemaOrgHTMLScope)


@register.inclusion_tag('djangocms_unimelb/404.html')
def unimelb_404():
    return {}


@register.inclusion_tag('djangocms_unimelb/500.html')
def unimelb_500():
    return {}
