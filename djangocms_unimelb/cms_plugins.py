from django.utils.translation import gettext_lazy as _
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from djangocms_link.cms_plugins import LinkPlugin
from djangocms_picture.cms_plugins import PicturePlugin
from djangocms_picture.models import Picture

from watson import search as watson

from . import models

module_name = 'Unimelb'

@plugin_pool.register_plugin
class UnimelbMessagesPlugin(CMSPluginBase):
    """
    Displays Django system messages using the Unimelb notices system.

    https://docs.djangoproject.com/en/3.2/ref/contrib/messages/
    https://web.unimelb.edu.au/components/notices
    """
    model = models.UnimelbMessages
    name = _('Messages')
    module = module_name
    render_template = "djangocms_unimelb/messages.html"


@plugin_pool.register_plugin
class UnimelbLeadPlugin(CMSPluginBase):
    """
    Displays text in a lead section.

    https://web.unimelb.edu.au/components/text
    """
    model = models.UnimelbLead
    name = _('Lead')
    module = module_name
    render_template = "djangocms_unimelb/lead.html"
    allow_children = True


@plugin_pool.register_plugin
class UnimelbNoticesPlugin(CMSPluginBase):
    """
    https://web.unimelb.edu.au/components/notices
    """
    model = models.UnimelbNotice
    name = _('Notice')
    module = module_name
    render_template = "djangocms_unimelb/notice.html"
    allow_children = True


@plugin_pool.register_plugin
class UnimelbTabPlugin(CMSPluginBase):
    """
    Creates a tab using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs
    """
    model = models.UnimelbTab
    name = _('Tabs')
    module = _('Unimelb')
    # change_form_template = 'djangocms_unimelb/admin/tabs.html'
    allow_children = True
    child_classes = ['UnimelbTabItemPlugin']
    render_template = "djangocms_unimelb/tab.html"

    fieldsets = [
        (None, {
            'fields': (
                ('tab_type','tab_index'),
            )
        }),
    ]

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin
class UnimelbTabItemPlugin(CMSPluginBase):
    """
    Creates an item in a tab using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs
    """
    model = models.UnimelbTabItem
    name = _('Tab item')
    module = _('Unimelb')
    # change_form_template = 'djangocms_unimelb/admin/tabs.html'
    allow_children = True
    parent_classes = ['UnimelbTabPlugin']
    render_template = "djangocms_unimelb/render_children.html"

    fieldsets = [
        (None, {
            'fields': (
                'tab_title',
            )
        }),
    ]

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin
class UnimelbStaffListingPlugin(CMSPluginBase):
    """
    Creates a staff listing using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs
    """
    model = models.UnimelbStaffListing
    name = _('Staff Listing')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbStaffMemberPlugin']
    render_template = "djangocms_unimelb/staff_listing.html"

    def get_render_template(self, context, instance, placeholder):
        if instance.listing_type == 'detailed':
            return 'djangocms_unimelb/staff_listing_detailed.html'
        elif instance.listing_type == 'min':
            return 'djangocms_unimelb/staff_listing_min.html'

        return 'djangocms_unimelb/staff_listing_default.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin
class UnimelbStaffMemberPlugin(CMSPluginBase):
    """
    Adds a staff member to a staff listing using the Unimelb theme.

    See https://web.unimelb.edu.au/components/tabs
    """
    model = models.UnimelbStaffMember
    name = _('Staff Member')
    module = _('Unimelb')
    allow_children = False
    parent_classes = ['UnimelbStaffListingPlugin']

    def get_render_template(self, context, instance, placeholder):
        parent_instance, plugin_class = instance.parent.get_plugin_instance()

        if parent_instance.listing_type == 'detailed':
            return 'djangocms_unimelb/staff_member_detailed.html'
        elif parent_instance.listing_type == 'min':
            return 'djangocms_unimelb/staff_member_min.html'

        return 'djangocms_unimelb/staff_member_default.html'


@plugin_pool.register_plugin
class UnimelbHeadingPlugin(CMSPluginBase):
    """
    See https://web.unimelb.edu.au/components/headings
    """
    model = models.UnimelbHeading
    name = _('Heading')
    module = _('Unimelb')
    allow_children = False
    render_template = "djangocms_unimelb/heading.html"

    def render(self, context, instance, placeholder):
        # If there are no heading then get it from the current page or current tab
        if not instance.text:
            # Check for tab item
            tab_item_plugin = instance.get_ancestors().filter(plugin_type='UnimelbTabItemPlugin').first()
            if tab_item_plugin:
                tab_item = tab_item_plugin.get_plugin_instance()[0]
                instance.text = str(tab_item)
            else:
                # If not tab item then user page title
                request = context['request']
                current_page = request.current_page
                instance.text = current_page.get_page_title()

        return super().render(context, instance, placeholder)



@plugin_pool.register_plugin
class UnimelbHeaderPlugin(CMSPluginBase):
    """
    https://web.unimelb.edu.au/components/headers
    """
    model = models.UnimelbHeader
    name = _('Header')
    module = _('Unimelb')
    allow_children = True
    render_template = "djangocms_unimelb/header.html"


@plugin_pool.register_plugin
class UnimelbBlockListingPlugin(CMSPluginBase):
    """
    Creates a block listing using the Unimelb theme.

    See https://web.unimelb.edu.au/components/block-listing
    """
    model = models.UnimelbBlockListing
    name = _('Block Listing')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbBlockListingItemNewsPlugin']
    render_template = "djangocms_unimelb/blocklisting.html"


@plugin_pool.register_plugin
class UnimelbBlockListingItemNewsPlugin(CMSPluginBase):
    """
    Creates a block listing using the Unimelb theme.

    See https://web.unimelb.edu.au/components/block-listing
    """
    model = models.UnimelbBlockListingItem
    name = _('Block Listing Item')
    module = _('Unimelb')
    allow_children = True
    parent_classes = ['UnimelbBlockListingPlugin']
    child_classes = ['UnimelbBlockListingImagePlugin']
    render_template = "djangocms_unimelb/blocklistingitemnews.html"
    fields = (
        'category',
        ('heading','minor_heading'),
        'text',
        'call_to_action',
        ('double', 'image_location'),
        ('meta_left', 'meta_right'),
        'picture',
        'external_picture',
        ('link_url', 'link_page'),
        ('file'),
        'link_target',
        'link_attributes',
    )
    # fieldsets = PicturePlugin.fieldsets.copy()
    fieldsets = [(
        None, {
            'fields': fields
        }
    )]


@plugin_pool.register_plugin
class UnimelbBlockListingImagePlugin(CMSPluginBase):
    """
    Creates a block listing using the Unimelb theme.
    """
    model = Picture
    name = _('Block Listing Image')
    module = _('Unimelb')
    render_template = "djangocms_unimelb/blocklistingimage.html"
    parent_classes = ['UnimelbBlockListingItemNews']
    allow_children = False


    fieldsets = [
        (None, {
            'fields': (
                'picture',
                'external_picture',
            )
        }),    
    ]


@plugin_pool.register_plugin
class UnimelbFooterPlugin(CMSPluginBase):
    """
    Creates a footer with a link to social media sites.
    """
    model = models.UnimelbFooter
    name = _('Footer')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbFooterLinkPlugin']
    render_template = "djangocms_unimelb/footer.html"


@plugin_pool.register_plugin
class UnimelbFooterLinkPlugin(CMSPluginBase):
    """
    Creates a footer with a link to social media sites.
    """
    model = models.UnimelbFooterLink
    name = _('Footer Link')
    module = _('Unimelb')
    parent_classes = ['UnimelbFooterPlugin']
    render_template = "djangocms_unimelb/footer_link.html"

    fields = (
        ('text', 'icon'),
        ('external_link', 'internal_link'),
    )
    fieldsets = LinkPlugin.fieldsets.copy()
    fieldsets[0] = (
        None, {
            'fields': fields
        }
    )

@plugin_pool.register_plugin
class UnimelbSearchFormPlugin(LinkPlugin):
    """
    Creates an input form.

    See https://web.unimelb.edu.au/components/search
    """
    model = models.UnimelbSearchForm
    name = _('Search Form')
    module = _('Unimelb')
    allow_children = False

    fields = (
        ('external_link', 'internal_link'),
    )
    fieldsets = LinkPlugin.fieldsets.copy()
    fieldsets[0] = (
        None, {
            'fields': fields
        }
    )

    # Need to override this function
    def get_render_template(self, context, instance, placeholder):
        return "djangocms_unimelb/search_form.html"


@plugin_pool.register_plugin
class UnimelbSearchResultsPlugin(CMSPluginBase):
    """
    Creates a search results box using the Unimelb theme.
    """
    model = models.UnimelbSearchResults
    name = _('Search Results')
    module = _('Unimelb')
    render_template = "djangocms_unimelb/search_results.html"

    def render(self, context, instance, placeholder):
        request = context['request']
        query_param = "q"

        query = request.GET.get(query_param, "").strip()
        qs = watson.search( query )
        paginator = Paginator( qs, instance.pagination )
        page_kwarg = 'page'
        page = request.GET.get(page_kwarg) or 1
        try:
            page_number = int(page)
        except ValueError:
            if page == 'last':
                page_number = paginator.num_pages
            else:
                raise Http404(_('Page is not “last”, nor can it be converted to an int.'))
        
        try:
            page_obj = paginator.page(page_number)
        except InvalidPage as e:
            raise Http404(_('Invalid page (%(page_number)s): %(message)s') % {
                'page_number': page_number,
                'message': str(e)
            })

        context.update(dict(
            query=query,
            search_results=page_obj.object_list,
            paginator=paginator,
            page_obj=page_obj,
        ))

        return super().render(context, instance, placeholder)


@plugin_pool.register_plugin
class UnimelbAccordionPlugin(CMSPluginBase):
    """
    Creates an accordion component using the Unimelb theme.

    https://web.unimelb.edu.au/components/accordion
    https://resources.web.unimelb.edu.au/web-content/creating-content/how-to-use-accordions-well
    """
    model = models.UnimelbAccordion
    name = _('Accordion')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbAccordionItemPlugin']
    render_template = "djangocms_unimelb/accordion.html"


@plugin_pool.register_plugin
class UnimelbAccordionItemPlugin(CMSPluginBase):
    """
    Creates an accordion component using the Unimelb theme.

    https://web.unimelb.edu.au/components/accordion
    https://resources.web.unimelb.edu.au/web-content/creating-content/how-to-use-accordions-well
    """
    model = models.UnimelbAccordionItem
    name = _('Accordion Item')
    module = _('Unimelb')
    allow_children = True
    parent_classes = ['UnimelbAccordionPlugin']
    render_template = "djangocms_unimelb/accordion_item.html"


@plugin_pool.register_plugin
class UnimelbTimelinePlugin(CMSPluginBase):
    """
    Creates an timeline component using the Unimelb theme.

    https://web.unimelb.edu.au/components/timeline
    https://www.w3schools.com/tags/tag_dl.asp
    """
    model = models.UnimelbTimeline
    name = _('Timeline')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbTimelineItemPlugin']
    render_template = "djangocms_unimelb/timeline.html"


@plugin_pool.register_plugin
class UnimelbTimelineItemPlugin(CMSPluginBase):
    """
    Creates an timeline component using the Unimelb theme.

    https://web.unimelb.edu.au/components/timeline
    https://www.w3schools.com/tags/tag_dl.asp
    """
    model = models.UnimelbTimelineItem
    name = _('Timeline Item')
    module = _('Unimelb')
    allow_children = True
    parent_classes = ['UnimelbTimelinePlugin']
    render_template = "djangocms_unimelb/timeline_item.html"


@plugin_pool.register_plugin
class UnimelbBlockquotePlugin(CMSPluginBase):
    """
    https://web.unimelb.edu.au/components/text
    """
    model = models.UnimelbBlockquote
    name = _('Blockquote')
    module = _('Unimelb')
    render_template = "djangocms_unimelb/blockquote.html"


@plugin_pool.register_plugin
class UnimelbFigureDivPlugin(CMSPluginBase):
    """
    https://web.unimelb.edu.au/components/figure
    """
    model = models.UnimelbFigureDiv
    name = _('Figure Div')
    module = _('Unimelb')
    render_template = "djangocms_unimelb/figure_div.html"
    allow_children = True


@plugin_pool.register_plugin
class UnimelbFigurePlugin(CMSPluginBase):
    """
    https://web.unimelb.edu.au/components/figure
    """
    model = models.UnimelbFigure
    name = _('Figure')
    module = _('Unimelb')
    render_template = "djangocms_unimelb/figure.html"
    text_enabled = True

    fieldsets = [
        (None, {
            'fields': (
                'picture',
                'external_picture',
                ('inset', 'confined','size'),
                'caption',
            )
        }),
        (_('Advanced settings'), {
            'classes': ('collapse',),
            'fields': (
                'template',
                'use_responsive_image',
                ('width', 'height'),
                'alignment',
                'attributes',
            )
        }),
        (_('Link settings'), {
            'classes': ('collapse',),
            'fields': (
                ('link_url', 'link_page'),
                'link_target',
                'link_attributes',
            )
        }),
        (_('Cropping settings'), {
            'classes': ('collapse',),
            'fields': (
                ('use_automatic_scaling', 'use_no_cropping'),
                ('use_crop', 'use_upscale'),
                'thumbnail_options',
            )
        })
    ]


@plugin_pool.register_plugin
class UnimelbPathfinderPlugin(CMSPluginBase):
    """
    Creates a pathfinder using the Unimelb theme.

    See https://web.unimelb.edu.au/components/pathfinder
    """
    model = models.UnimelbPathfinder
    name = _('Pathfinder')
    module = _('Unimelb')
    allow_children = True
    child_classes = ['UnimelbPathfinderBoxPlugin']
    render_template = "djangocms_unimelb/pathfinder.html"

    def render(self, context, instance, placeholder):
        children_count = len(instance.child_plugin_instances)

        # If there are no children pathfinder boxes then make some pseudo ones from the page hierarchy
        if children_count == 0:
            request = context['request']
            current_page = request.current_page
            instance.boxes = []
            for child in current_page.get_child_pages():
                instance.boxes.append(dict(
                    heading=child.get_menu_title(),
                    text=child.get_meta_description(),
                    get_link=f"/{child.get_path()}",
                    button="View",
                ))
            children_count = len(instance.boxes)

        number = children_count
        number = max(number, 2)
        number = min(number, 4)
        context.update({'number': number})
        return super().render(context, instance, placeholder)


@plugin_pool.register_plugin
class UnimelbPathfinderBoxPlugin(LinkPlugin):
    """
    Creates a pathfinder box using the Unimelb theme.

    See https://web.unimelb.edu.au/components/pathfinder
    """
    model = models.UnimelbPathfinderBox
    name = _('Pathfinder Box')
    module = _('Unimelb')
    parent_classes = ['UnimelbPathfinderPlugin']
    allow_children = False

    fields = (
        'heading',
        'text',
        'button',
        ('external_link', 'internal_link'),
    )
    fieldsets = LinkPlugin.fieldsets.copy()
    fieldsets[0] = (
        None, {
            'fields': fields
        }
    )

    # Need to override this function
    def get_render_template(self, context, instance, placeholder):
        return "djangocms_unimelb/pathfinder_box.html"


